package util;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.Scanner;
import javax.swing.JFileChooser;
import java.io.File;
import javax.swing.JOptionPane;

public class UtilConsole {

	static Scanner sc = new Scanner(System.in);

	public static String demanarString(String etiqueta) {
		String cadena = " ";
		boolean isString = false;

		do {
			System.out.print(etiqueta);
			if (sc.hasNext()) {
				cadena = sc.nextLine();
				isString = true;
			}else {
				sc.nextLine();
			}

		} while (!isString);

		return cadena;
	}

	public static int demanarInt(String etiqueta) {
		int entero = 0;
		boolean isEntero = false;

		do {
			System.out.print(etiqueta);
			if (sc.hasNextInt()) {
				entero = Integer.parseInt(sc.nextLine());
				isEntero = true;
			}else {
				sc.nextLine();
			}


		} while (!isEntero);

		return entero;
	}

	public static boolean demanarBoolean(String etiqueta) {
		String bool;
		do {
			bool = demanarString(etiqueta);
		} while (!bool.matches("[0-1]{1}"));
		if (bool.equals("0")) {
			return false;
		}else {
			return true;
		}
	}

	public static double demanarDouble(String etiqueta) {
		double decimal = 0;
		boolean isDouble = false;
		do {
			System.out.print(etiqueta);
			if (sc.hasNextDouble()) {
				decimal = sc.nextDouble();
				sc.nextLine();
				isDouble = true;
			}else {
				sc.nextLine();
			}

		} while (!isDouble);

		return decimal;
	}

	public static String demanarDNI(String etiqueta) {
		String DNI;
		boolean dniOK = false;
		do {
			DNI = demanarString(etiqueta);
			dniOK = validarDNI(DNI);
		} while (!dniOK);
		return DNI;
	}

	public static boolean validarDNI(String dni) {
		String lletres = "TRWAGMYFPDXBNJZSQVHLCKET";

		if(dni.matches("[0-9]{7,8}[A-Z a-z]")){
			int numDNI = Integer.parseInt(dni.substring(0,dni.length()-1));
			char lletraDNI = dni.charAt(dni.length()-1);
			int mod = numDNI % 23;
			if(lletres.charAt(mod) == lletraDNI) return true;
		}

		return false;
	}

	public static LocalDate demanarData(String etiqueta) {

		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy");
		String dataString = "";
		LocalDate data = null;
		boolean isDate = false;

		do {
			dataString = demanarString(etiqueta);
			try {
				data = LocalDate.parse(dataString, dtf);
				isDate = true;
			} catch (DateTimeParseException e) {
				System.out.println("Fecha incorrecta, formato correcto dd/mm/aaaa");
			}
		}while(!isDate);

		return data;
	}


	public static String demanarEmail(String etiqueta) {
		String email;
		do {
			email = demanarString(etiqueta);
		}while (!email.matches("^(.+)@(.+)$"));

		return email;
	}

	public static String demanarCP(String etiqueta) {
		String cp;
		do {
			cp = demanarString(etiqueta);
		} while (!cp.matches("[0-9]{5}"));
		return cp;
	}

	public static String demanarTelefono(String etiqueta) {
		String telefono;
		do {
			System.out.println(etiqueta);
			telefono = sc.nextLine();
		} while (!telefono.matches("[6-7][0-9]{8}"));
		return telefono;
	}
	
	 public static String demanarRuta() {
	        JFileChooser fileChooser = new JFileChooser();
	        int resultat = fileChooser.showOpenDialog(null);
	        if (resultat == JFileChooser.APPROVE_OPTION) { // Si l'usuari selecciona un arxiu
	            File arxiuSeleccionat = fileChooser.getSelectedFile(); // Obté l'arxiu seleccionat
	            return arxiuSeleccionat.getAbsolutePath(); // Retorna la ruta de l'arxiu
	        }
	        return null; // Si l'usuari cancel·la el diàleg, retorna null
	    }
	 
	 public static boolean confirmar(String missatge) {
		    int opcio = JOptionPane.showConfirmDialog(null, missatge, "Confirmació", JOptionPane.YES_NO_OPTION);
		    if (opcio == JOptionPane.YES_OPTION) {
		        return true;
		    } else {
		        return false;
		    }
		}
}
