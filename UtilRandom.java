package util;

import java.util.Random;
import java.awt.Color;
import java.time.LocalDate;
import java.util.concurrent.ThreadLocalRandom;

public class UtilRandom {
	
	public static int generarNumero(int minim, int maxim) {
	    Random random = new Random();
	    int numeroAleatori = random.nextInt((maxim - minim) + 1) + minim;
	    return numeroAleatori;
	}

	public static Color generarColor() {
	    Random random = new Random();
	    float r = random.nextFloat();
	    float g = random.nextFloat();
	    float b = random.nextFloat();
	    Color colorAleatorio = new Color(r, g, b);
	    return colorAleatorio;
	}
	
	public static boolean generarBoolean(double probabilitat) {
	    Random random = new Random();
	    boolean resultat = random.nextDouble() < probabilitat;
	    return resultat;
	}

	public static String generarTelefon() {
	    Random random = new Random();
	    StringBuilder builder = new StringBuilder();
	    builder.append("6"); // Codi de Espanya
	    for (int i = 0; i < 8; i++) { 
	        int numeroAleatori = random.nextInt(10);
	        builder.append(numeroAleatori);
	    }
	    String telefonoAleatori = builder.toString();
	    return telefonoAleatori;
	}
	
	public static LocalDate generarData() {
	    long minDay = LocalDate.of(1900, 1, 1).toEpochDay();
	    long maxDay = LocalDate.now().toEpochDay();
	    long randomDay = ThreadLocalRandom.current().nextLong(minDay, maxDay);
	    LocalDate fechaAleatoria = LocalDate.ofEpochDay(randomDay);
	    return fechaAleatoria;
	}
	
	public static String generarUsuari() {
	    String alfabeto = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
	    Random rand = new Random();
	    StringBuilder builder = new StringBuilder();
	    for (int i = 0; i < 8; i++) {
	        int indice = rand.nextInt(alfabeto.length());
	        builder.append(alfabeto.charAt(indice));
	    }
	    String usuarioAleatorio = builder.toString();
	    return usuarioAleatorio;
	}
}
