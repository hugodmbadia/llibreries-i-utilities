package util;
import java.util.Random;

public class Enums {
	
	public enum DiaSetmana {
	    DILLUNS, DIMARTS, DIMECRES, DIJOUS, DIVENDRES, DISSABTE, DIUMENGE;
	    
	    private static final Random random = new Random();
	    
	    public int generarNumero() { //Utilitzem el mètode generarNumero de l'UtilRandom
	        return random.nextInt(7) + 1;
	    }
	}
	
	public enum Color {
	    VERMELL, BLAU, VERD, GROC, LILA, BLANC, NEGRE, TARONJA, MARRO, GRIS, ROSA;
	}
	
	public enum Direccio {
	    NORD, SUD, EST, OEST;
	}



}
